public class HomeController : Controller
{
    public ActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Encrypt(string recipientPublicKey, string message)
    {
        string encryptedFilePath = "path_to_store_encrypted_file.pgp";
        PGPService.EncryptMessage(recipientPublicKey, message, encryptedFilePath);
        return File(encryptedFilePath, "application/octet-stream", "encrypted_message.pgp");
    }

    [HttpPost]
    public ActionResult Decrypt(string privateKey, string passphrase, HttpPostedFileBase encryptedFile)
    {
        string encryptedFilePath = "path_to_store_uploaded_encrypted_file.pgp";
        encryptedFile.SaveAs(encryptedFilePath);

        string decryptedMessage = PGPService.DecryptMessage(privateKey, passphrase, encryptedFilePath);
        ViewBag.DecryptedMessage = decryptedMessage;

        return View("Decrypted");
    }
}
