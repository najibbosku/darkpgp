using OpenPGP;
using System.IO;

public class PGPService
{
    public static void EncryptMessage(string recipientPublicKey, string message, string encryptedFilePath)
    {
        using (PGPLib pgp = new PGPLib())
        {
            pgp.EncryptFile(
                inputFile: new MemoryStream(Encoding.UTF8.GetBytes(message)),
                publicKeyFile: new MemoryStream(Encoding.UTF8.GetBytes(recipientPublicKey)),
                outputFile: new FileStream(encryptedFilePath, FileMode.Create),
                armor: true,
                withIntegrityCheck: true
            );
        }
    }

    public static string DecryptMessage(string privateKey, string passphrase, string encryptedFilePath)
    {
        using (PGPLib pgp = new PGPLib())
        {
            using (MemoryStream decryptedStream = new MemoryStream())
            {
                pgp.DecryptFile(
                    inputFile: new FileStream(encryptedFilePath, FileMode.Open),
                    privateKeyFile: new MemoryStream(Encoding.UTF8.GetBytes(privateKey)),
                    passPhrase: passphrase,
                    outputFile: decryptedStream
                );

                decryptedStream.Position = 0;
                using (StreamReader reader = new StreamReader(decryptedStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
